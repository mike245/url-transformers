package com.example.transformer;

import java.util.Collection;

public class BaseEnumTransformer<E> {

    protected Collection<Class<? extends E>> classes;
    private Enum<?> result;

    protected BaseEnumTransformer(Collection<Class<? extends E>> classes) {
        this.classes = classes;
    }

    public Enum<?> transform(String name) {
        if (result == null) {
            for (Class<? extends E> clazz : classes) {
                try {
                    result = Enum.valueOf((Class)clazz, name);
                } catch (IllegalArgumentException e) {
                    //omit
                }
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("No enum constant " + name);
        }
        return result;
    }
}
