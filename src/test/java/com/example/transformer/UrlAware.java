package com.example.transformer;

public interface UrlAware {

    String getFullUrl();
}
