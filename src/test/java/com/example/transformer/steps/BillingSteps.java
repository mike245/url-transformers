package com.example.transformer.steps;

import com.example.transformer.BillingUrl;
import com.example.transformer.TestConfig;
import com.example.transformer.UrlTransformer;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = TestConfig.class) // This class would contain your Spring configuration
public class BillingSteps {

    @ParameterType("B_URL_1|B_URL_2")
    public BillingUrl bUrl(String url) {
        UrlTransformer transformer = new UrlTransformer(BillingUrl.class);
        return (BillingUrl) transformer.transform(url);
    }

    @Given("Call url {bUrl}")
    public void call_bUrl(BillingUrl bUrl) {
        System.out.println(bUrl.getFullUrl());
    }
}
