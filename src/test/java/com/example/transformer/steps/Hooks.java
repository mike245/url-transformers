package com.example.transformer.steps;

import io.cucumber.java.After;
import io.cucumber.java.AfterAll;

public class Hooks {

  @After
  public void afterTest() {
    System.out.println("AfterTestHook");
  }

  @AfterAll
  public static void afterAll() {
    System.out.println("AfterAllHook");
  }
}
