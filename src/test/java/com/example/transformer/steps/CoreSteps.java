package com.example.transformer.steps;

import com.example.transformer.BillingUrl;
import com.example.transformer.CoreUrl;
import com.example.transformer.TestConfig;
import com.example.transformer.UrlTransformer;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.test.context.ContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration(classes = TestConfig.class) // This class would contain your Spring configuration
public class CoreSteps {

    @ParameterType("C_URL_1|C_URL_2")
    public CoreUrl cUrl(String url) {
        UrlTransformer transformer = new UrlTransformer(CoreUrl.class);
        return (CoreUrl) transformer.transform(url);
    }

    @Given("Call url {cUrl}")
    public void call_cUrl(CoreUrl cUrl) {
        System.out.println(cUrl.getFullUrl());
    }
}
