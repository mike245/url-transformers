package com.example.transformer;

public enum BillingUrl implements UrlAware {

    B_URL_1("/billing_url1"),
    B_URL_2("/billing_url2");

    private final String url;

    BillingUrl(String url) {
        this.url = url;
    }

    @Override
    public String getFullUrl() {
        return "https://billing.com%s".formatted(url);
    }
}
