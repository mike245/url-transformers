package com.example.transformer;

import java.util.Collection;
import java.util.List;

public class UrlTransformer extends BaseEnumTransformer<UrlAware> {

    public UrlTransformer(Class<? extends UrlAware> ...classes) {
        super(List.of(classes));
    }
}
