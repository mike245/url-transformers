package com.example.transformer;

public enum CoreUrl implements UrlAware {

    C_URL_1("/core_url1"),
    C_URL_2("/core_url2");

    private final String url;

    CoreUrl(String url) {
        this.url = url;
    }

    @Override
    public String getFullUrl() {
        return "https://core.com%s".formatted(url);
    }
}
