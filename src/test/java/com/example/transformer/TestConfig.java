package com.example.transformer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.example.transformer")
public class TestConfig {
    // Additional beans and configuration can be set up here

  @Bean
  DestroyBean destroyBean() {
    return new DestroyBean();
  }
}