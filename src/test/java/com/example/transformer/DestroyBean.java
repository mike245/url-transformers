package com.example.transformer;

import javax.annotation.PreDestroy;

public class DestroyBean {

  @PreDestroy
  public void preDestroy() {
    System.out.println("preDestroy");
  }
}
