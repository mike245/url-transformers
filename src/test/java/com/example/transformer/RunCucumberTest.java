package com.example.transformer;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", glue = "com.example.transformer.steps")
public class RunCucumberTest {
    // no need to add code here, just annotations
}
